<?php

class GraphDocOutputGraphviz {
  /**
   * @var string
   */
  protected $nl;

  function __construct($newLine = "\n") {
    $this->nl = $newLine;
  }

  /**
   * Render a graph (to Graphviz notation).
   *
   * @param \GraphDocGraph $graph
   * @param int $level
   * @return string
   */
  public function renderGraph(GraphDocGraph $graph, $level = 0) {
    $indent = $this->generateIndent($level);
    $subIndent = $this->generateIndent($level+1);

    $output = $indent . "digraph G {" . $this->nl;
    $output .= $subIndent . 'compound = true' . $this->nl;

    foreach($graph->getVertices() as $vertex) {
      $output .= $this->renderVertex($vertex, $level+1);
    }

    $output .= $indent . $this->nl;

    foreach($graph->getEdges() as $edge) {
      $output .= $this->renderEdge($edge, $level+1);
    }

    $output .= $indent . "}" . $this->nl;

    return $output;
  }

  /**
   * @param string $renderedGraph
   * @return string
   * @throws Exception
   */
  public function convertToImage($renderedGraph) {
    exec("(echo -e " . escapeshellarg($renderedGraph) . ' | dot -Tsvg)', $out, $returnValue);
    $out = implode("\n", $out);

    if ($returnValue != 0) {
      throw new Exception("Graphviz exited with code " . $returnValue);
    }

    return $out;
  }

  /**
   * Render a vertex
   *
   * @param \GraphDocVertex $vertex
   * @param int $level
   * @return string
   */
  protected function renderVertex(GraphDocVertex $vertex, $level = 0) {
    // Do not show hidden vertices.
    if (!$vertex->isVisible()) {
      return '';
    }

    // Render vertice as a feature if it contains sub-vertices or sub-edges.
    if (count($vertex->getVertices()) || count($vertex->getEdges())) {
      return $this->renderVertexFeature($vertex, $level);
    }

    switch ($vertex->getType()) {
      case 'feature':
        return $this->renderVertexFeature($vertex, $level);
      default:
        return $this->renderVertexSubFeature($vertex, $level);
    }
  }

  /**
   * @param \GraphDocVertex $vertex
   * @param int $level
   * @return string
   */
  protected function renderVertexFeature(GraphDocVertex $vertex, $level = 0) {
    $indent = $this->generateIndent($level);
    $subIndent = $this->generateIndent($level+1);

    $name = $this->getFullName($vertex);
    $output = $indent . 'subgraph ' . $name . "{" . $this->nl;
    $output .= $subIndent . 'label = "' . $this->renderVertexName($vertex) . '"' . $this->nl;
    if ($vertex->getDescription()) {
      $output .= $subIndent . 'xlabel = "' . $vertex->getDescription() . '"' . $this->nl;
    }

    // Add a placeholder vertex, so that the cluster is shown even when it has
    // no real children. The placeholder is also used when connecting and edge
    // straight to a cluster, which does not work directly in Graphviz.
    $output .= $this->renderVertex(new GraphDocVertex($vertex->getMachineName() . '_placeholder', 'placeholder'));

    // Render subvertices
    $subVertices = $vertex->getVertices();
    foreach($subVertices as $subVertex) {
      $output .= $this->renderVertex($subVertex, $level+1);
    }

    $output .= $indent . "}" . $this->nl;

    // Render subedges
    foreach($vertex->getEdges() as $edge) {
      $output .= $this->renderEdge($edge, $level+1);
    }

    return $output;
  }

  /**
   * @param \GraphDocVertex $vertex
   * @param int $level
   * @return string
   */
  protected function renderVertexSubFeature(GraphDocVertex $vertex, $level = 0) {
    $indent = $this->generateIndent($level);

    $shape = "doubleoctagon";
    $style = "filled";

    switch ($vertex->getType()) {
      case 'node':
        $shape = "oval";
        break;
      case 'views_view':
        $shape = "tab";
        break;
      case 'feeds_importer':
        $shape = "larrow";
        break;
      case 'search_api_index':
        $shape = "octagon";
        break;
      case 'field_base':
        $shape = 'cds';
        break;
      case 'field_instance':
        $shape = 'cds';
        break;
      case 'taxonomy':
        $shape = 'diamond';
        break;
      case 'placeholder':
        $shape = "point";
        $style = "invis";
        break;
    }

    return $indent . '"' . $this->getFullName($vertex) . '" [label = "' . $vertex->getMachineName() . '" shape = ' . $shape . ' style = ' . $style . ' fillcolor=yellow color = black tooltip = "' . $vertex->getType() . '"]' . $this->nl;
  }

  /**
   * Render an edge.
   *
   * @param \GraphDocEdge $edge
   * @param int $level
   * @return string
   */
  protected function renderEdge(GraphDocEdge $edge, $level = 0) {
    // Do not show hidden edges.
    if (!$edge->isVisible()) {
      return '';
    }

    $indent = $this->generateIndent($level);
    $renderableEdge = array('extra' => array());

    $renderableEdge = $this->getEdgePoint($renderableEdge, $edge->getFrom(), 'ltail');
    $renderableEdge = $this->getEdgePoint($renderableEdge, $edge->getTo(), 'lhead');

    // Render extra attributes if needed.
    $extra = '';
    if (count($renderableEdge['extra'])) {
      foreach ($renderableEdge['extra'] as $key => &$value) {
        $value = $key . ' = "' . $value . '"';
      }
      $extra = ' [' . implode(" ", $renderableEdge['extra']) . ']';
    }


    return $indent . '"' . $renderableEdge['ltail'] . '" -> "' . $renderableEdge['lhead'] . '"' . $extra . ';' . $this->nl;
  }

  /**
   * Get edge point attributes for rendering.
   *
   * @param array $renderableEdge
   * @param \GraphDocVertex $vertex
   * @param $endType
   * @return array
   */
  protected function getEdgePoint(array $renderableEdge, GraphDocVertex $vertex, $endType) {
    $endType = ($endType == 'ltail') ? $endType : 'lhead';

    $vertexName = $this->getFullName($vertex);

    // If the vertice is a cluster, use internal placeholder for an edge point.
    if (count($vertex->getVertices())) {
      $placeholder = new GraphDocVertex($vertex->getMachineName() . '_placeholder', 'placeholder');
      $renderableEdge[$endType] = $this->getFullName($placeholder);
      $renderableEdge['extra'] = array($endType => $vertexName);
    }
    else {
      $renderableEdge[$endType] = $vertexName;
    }

    return $renderableEdge;
  }

  /**
   * Get a full name for the vertex that can be used in a graph.
   *
   * @param \GraphDocVertex $vertex
   * @return string
   */
  protected function getFullName(GraphDocVertex $vertex) {
    $clusterPrefix = '';
    if(count($vertex->getVertices()) || count($vertex->getEdges())) {
      $clusterPrefix = 'cluster_';
    }

    return $clusterPrefix . $vertex->getType() . '_' . $vertex->getMachineName();
  }

  /**
   * Helper function for rendering vertex name.
   *
   * @param \GraphDocVertex $vertex
   * @return string
   */
  protected function renderVertexName(GraphDocVertex $vertex) {
    if (!$vertex->getHumanName()) {
      return $vertex->getMachineName();
    }

    return $vertex->getHumanName() . ' (' . $vertex->getMachineName() . ')';
  }

  /**
   * Generate indentation based on the depth level.
   *
   * @param int $level
   * @return string
   */
  protected function generateIndent($level) {
    $output = '';
    for($i=0; $i<$level; $i++) {
      $output .= '  ';
    }

    return $output;
  }
}
