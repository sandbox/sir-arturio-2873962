<?php

class GraphDocEdge {
  /**
   * @var \GraphDocVertex $from
   */
  protected $from;

  /**
   * @var \GraphDocVertex $from
   */
  protected $to;

  /**
   * @var string $type
   */
  protected $type;

  /**
   * @var string $machineName
   */
  protected $machineName;

  /**
   * @var bool $visible
   */
  protected $visible;

  public function __construct(GraphDocVertex $from, GraphDocVertex $to, $type = '', $machineName = '', $visible = TRUE) {
    $this->from = $from;
    $this->to = $to;
    $this->type = $type;
    $this->machineName = $machineName;
    $this->visible = $visible;
  }

  /**
   * @param bool $visibility
   */
  public function setEndpointVisibility($visibility) {
    $visibility = $visibility? TRUE : FALSE;
    $this->from->setVisible($visibility);
    $this->to->setVisible($visibility);
  }

  /**
   * @return \GraphDocVertex
   */
  public function getFrom() {
    return $this->from;
  }

  /**
   * @param \GraphDocVertex $from
   */
  public function setFrom($from) {
    $this->from = $from;
  }

  /**
   * @return \GraphDocVertex
   */
  public function getTo() {
    return $this->to;
  }

  /**
   * @param \GraphDocVertex $to
   */
  public function setTo($to) {
    $this->to = $to;
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param string $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * @return string
   */
  public function getMachineName() {
    return $this->machineName;
  }

  /**
   * @param string $machineName
   */
  public function setMachineName($machineName) {
    $this->machineName = $machineName;
  }

  /**
   * @return bool
   */
  public function isVisible() {
    return $this->visible;
  }

  /**
   * @param bool $visible
   */
  public function setVisible($visible) {
    $this->visible = $visible;
  }
}
