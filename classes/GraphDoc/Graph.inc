<?php

class GraphDocGraph {
  /**
   * @var GraphDocVertex[][] $vertices
   */
  protected $vertices = array();

  /**
   * @var GraphDocEdge[][] $edges
   */
  protected $edges = array();

  /**
   * Add a vertex to the graph.
   *
   * @param GraphDocVertex $vertex
   */
  public function addVertex(GraphDocVertex $vertex) {
    $type = $vertex->getType() ? $vertex->getType() : 'no-type';

    if (!isset($this->vertices[$type])) {
      $this->vertices[$type] = array();
    }

    if ($vertex->getMachineName()) {
      $this->vertices[$type][$vertex->getMachineName()] = $vertex;
    }
    else {
      $this->vertices[$type][] = $vertex;
    }
  }

  /**
   * Ensure that the given vertex exists as a subvertex.
   *
   * @param \GraphDocVertex $vertex
   * @return \GraphDocVertex
   */
  public function ensureVertex(GraphDocVertex $vertex) {
    try {
      return $this->getVertex($vertex->getType(), $vertex->getMachineName());
    }
    catch (Exception $e) {
      $this->addVertex($vertex);
      return $vertex;
    }
  }

  /**
   * Add an edge to the graph.
   * @param GraphDocEdge $edge
   */
  public function addEdge(GraphDocEdge $edge) {
    $type = $edge->getType() ? $edge->getType() : 'no-type';

    if (!isset($this->edges[$type])) {
      $this->edges[$type] = array();
    }

    if ($edge->getMachineName()) {
      $this->edges[$type][$edge->getMachineName()] = $edge;
    }
    else {
      $this->edges[$type][] = $edge;
    }
  }

  /**
   * @return \GraphDocVertex[]
   */
  public function getVertices() {
    $result = array();
    foreach($this->vertices as $typeArray) {
      $result = array_merge($result, array_values($typeArray));
    }

    return $result;
  }

  /**
   * @return \GraphDocEdge[]
   */
  public function getEdges() {
    $result = array();
    foreach($this->edges as $typeArray) {
      $result = array_merge($result, array_values($typeArray));
    }

    return $result;
  }

  /**
   * Get vertex by type and machinename.
   *
   * @param string $type
   * @param string $machineName
   * @return \GraphDocVertex
   * @throws \Exception
   */
  public function getVertex($type, $machineName) {
    $type = ($type) ? $type : 'no-type';

    if (!isset($this->vertices[$type][$machineName])) {
      throw new Exception('Vertex "' . $machineName . '" of type "' . $type . '" not found.');
    }

    return $this->vertices[$type][$machineName];
  }

  /**
   * Get vertex by type and key.
   *
   * @param string $type
   * @param string $machineName
   * @return \GraphDocEdge
   * @throws \Exception
   */
  public function getEdge($type = NULL, $machineName) {
    $type = ($type) ? $type : 'no-type';

    if (!isset($this->edges[$type][$machineName])) {
      throw new Exception('Edge "' . $machineName . '" of type "' . $type . '" not found.');
    }

    return $this->edges[$type][$machineName];
  }
}
