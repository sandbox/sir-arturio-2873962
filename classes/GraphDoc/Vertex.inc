<?php

class GraphDocVertex extends GraphDocGraph {
  /**
   * @var string $machineName
   */
  protected $machineName;

  /**
   * @var string $type
   */
  protected $type;

  /**
   * @var string $humanName
   */
  protected $humanName;

  /**
   * @var string $description
   */
  protected $description;

  /**
   * @var bool $visible
   */
  protected $visible;

  /**
   * @param string $machineName
   * @param string $type
   * @param string $humanName
   * @param string $description
   * @param bool $visible;
   */
  public function __construct($machineName, $type, $humanName = NULL, $description = NULL, $visible = TRUE) {
    $this->machineName = $machineName;
    $this->type = $type;
    $this->humanName = $humanName;
    $this->description = $description;
    $this->visible = $visible;
  }

  /**
   * @return string
   */
  public function getMachineName() {
    return $this->machineName;
  }

  /**
   * @param string $machineName
   */
  public function setMachineName($machineName) {
    $this->machineName = $machineName;
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param string $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * @return string
   */
  public function getHumanName() {
    return $this->humanName;
  }

  /**
   * @param string $humanName
   */
  public function setHumanName($humanName) {
    $this->humanName = $humanName;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param string $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * @return bool
   */
  public function isVisible() {
    return $this->visible;
  }

  /**
   * @param bool $visible
   */
  public function setVisible($visible) {
    $this->visible = $visible;
  }
}
